const express = require('express');
const user = require('../middlewares/v1/user');
const router = express.Router();

router.get('/', (req, res) => {
	res.json({ message: 'Welcome to Sky Bet' });
});

router.put('/user', user.createUser);
router.post('/user/:id', user.updateUser);
router.delete('/user/:id', user.deleteUser);
router.get('/users', user.getUsers);

module.exports = router;