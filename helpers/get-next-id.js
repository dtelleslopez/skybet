'use strict';

module.exports = function(users) {
	return (users.sort((a, b) => { return a.id - b.id; }).slice(-1).map((user) => { return user.id; }).pop() + 1) || 0;
};