const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();

chai.use(chaiHttp);

describe('Users', () => {
    describe('/GET users', () => {
        it('it should GET all the users', (done) => {
            chai.request(server)
                .get('/users')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('users');
                    done();
                });
        });
    });

    describe('/PUT user', () => {
        it('it should PUT a user ', (done) => {
            let user = {
                id: 6,
                name: "Kim",
                surname: "Williams"
            }

            chai.request(server)
                .put('/user')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('users');
                    done();
                });
        });
    });

    describe('/POST user', () => {
        it('it should POST a user ', (done) => {
            let user = {
                id: 6,
                name: "Kim",
                surname: "Kardashian"
            }

            chai.request(server)
                .post('/user/6')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('users');
                    done();
                });
        });
    });

    describe('/DELETE user', () => {
        it('it should DELETE a user ', (done) => {
            chai.request(server)
                .delete('/user/6')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('users');
                    done();
                });
        });
    });
});