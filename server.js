const express = require('express');
const port = process.env.port || 3000;
const routes = require('./routes');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const app = express();

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));
app.use('/', routes.v1);

app.listen(port, () => {
    console.log('Magic happens on port ' + port);
});

module.exports = app;
