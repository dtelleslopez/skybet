const fs = require('fs');
const file = './data/users.json';
const getNextId = require('../../helpers/get-next-id');

let users = JSON.parse(fs.readFileSync(file, 'utf8'));

exports.createUser = (req, res) => {
	users = JSON.parse(fs.readFileSync(file, 'utf8'));
    
    let user = {
    	id: getNextId(users),
    	name: req.body.name,
    	surname: req.body.surname
    };

    users.push(user);

    fs.writeFileSync(file, JSON.stringify(users), 'utf8');

    return res.json({ users: users });
};

exports.updateUser = (req, res) => {
	users = JSON.parse(fs.readFileSync(file, 'utf8'));

    for (let i = users.length - 1; i >= 0; i--) {
    	if (users[i].id === parseInt(req.params.id)) {
    		users[i].name = req.body.name || users[i].name;
    		users[i].surname = req.body.surname || users[i].surname;
    		break;
    	}
    }

    fs.writeFileSync(file, JSON.stringify(users), 'utf8');

    return res.json({ users: users });
};

exports.deleteUser = (req, res) => {
	users = JSON.parse(fs.readFileSync(file, 'utf8'));

    for (var i = users.length - 1; i >= 0; i--) {
    	if (users[i].id === parseInt(req.params.id)) {
    		users.splice(i, 1);
    		break;
    	}
    }

    fs.writeFileSync(file, JSON.stringify(users), 'utf8');

    return res.json({ users: users });
};

exports.getUsers = (req, res) => {
	return res.json({ users: users });
};